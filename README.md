Der IOCCC: Wie man mit schlechtem Code gewinnt
==============================================

Vortrag im Datengarten 106

* [Ankündigung](https://berlin.ccc.de/datengarten/106/)
* [Folien (PDF)](https://gitlab.com/v0g/Vortragsfolien-Datengarten-106-Der-IOCCC/raw/master/Vortragsfolien-Datengarten-106-Der-IOCCC.pdf)
* [Video-Aufzeichnung](https://media.ccc.de/v/dg-106)
