NAME = Vortragsfolien-Datengarten-106-Der-IOCCC
TMPFILES = $(patsubst %,$(NAME).%,aux log nav out snm synctex.gz toc vrb)

.PHONY: default
default: $(NAME).pdf

$(NAME).pdf: $(NAME).tex by-sa.eps
	rm -f $(TMPFILES) $(NAME).pdf
	xelatex $(NAME).tex
	xelatex $(NAME).tex
	xelatex $(NAME).tex
	rm -f $(TMPFILES)
